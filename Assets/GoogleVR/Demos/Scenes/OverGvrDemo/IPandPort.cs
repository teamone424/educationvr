﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;

public class IPandPort : MonoBehaviour {
	private void Awake(){
		DontDestroyOnLoad(gameObject);
		if ((textFieldString = PlayerPrefs.GetString("PrevIPandPort")) == ""){
			textFieldString = "127.0.0.1:8080";
			
			
			
		}
	
		gameObject.tag = "AddressKeeper";
		
	}


	public string getServerIPandPort(){

		return textFieldString;
		
	}

	
	
	private GUIStyle textGuiStyle;

	private void Start(){
		textGuiStyle = new GUIStyle();
		textGuiStyle.fontSize = 20;
		
	}


	private bool enabledGui = true;
	private string textFieldString = "127.0.0.1:8080";


	public float sizeTextBoxX = 100;
	public float sizeTextBoxY = 50;



	private void OnGUI() {

		if (enabledGui){

			GUI.Label(new Rect(Screen.width / 2.0f - 50, Screen.height / 2.0f - 25, 140, 50), "Enter Details ");
			textFieldString = GUI.TextField(new Rect(Screen.width / 2.0f - sizeTextBoxX/2, Screen.height / 2.0f + 25, sizeTextBoxX,sizeTextBoxY),textFieldString,textGuiStyle);

			if (GUI.Button(new Rect(Screen.width / 2.0f - 50, Screen.height / 2.0f + 25+sizeTextBoxY, 100, 50), "Enter")){
				enabledGui = false;
				
				PlayerPrefs.SetString("PrevIPandPort",textFieldString);
				SceneManager.LoadScene("GVRDemo");
			}
		}

	}

}
