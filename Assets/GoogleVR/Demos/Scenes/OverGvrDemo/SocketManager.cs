﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using SocketIO;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.VR;

public class SocketManager : MonoBehaviour{





	private SocketIOComponent mySocket;
	private bool emittedIdentity = false;


	public GameObject questionMark;


	
	private InteractQuestionMark iqm = null;
	

	public void pingServer(){
		if (emittedIdentity )
			mySocket.Emit("query1");
			

	}

	void Awake(){

		gameObject.tag = "SocketManager";


	}


	// Use this for initialization
	void Start () {
		
		var serverIPandPort = "127.0.0.1:8080";
		{
			var addrObj = GameObject.FindGameObjectWithTag("AddressKeeper");
			if (addrObj != null){
				serverIPandPort =  addrObj.GetComponent<IPandPort>().getServerIPandPort();

			}
		}
		
		Debug.Log("serve ip and port is "+serverIPandPort);

		string url = "ws://" + serverIPandPort + "/socket.io/?EIO=4&transport=websocket";
		

		
		
		iqm = questionMark.GetComponent<InteractQuestionMark>();

		
		
		{
			mySocket = SocketIOComponent.addSocketIOComponent(gameObject, url);
			Assert.IsNotNull(mySocket);

			if (mySocket == null){
				Application.Quit();
			}
			
			mySocket.Connect();
			mySocket.On("open", OnConnected);
			mySocket.On("identify",OnIdentify);
			mySocket.On("answer",OnAnswer);
		}



	}
	void OnConnected(SocketIOEvent e){
		
		Debug.Log("connected to server");
		
		
		

	}

	void OnIdentify(SocketIOEvent e){
		if (!emittedIdentity){
			
			
			emittedIdentity = true;
			mySocket.Emit("identification", new JSONObject("{\"name\":\"John\"}"));
			
		}
	}

	void OnAnswer(SocketIOEvent e){
		var d = e.data;
		iqm.OnRecieveCallback(d["reply"].str);


	}
	
}
