﻿using System.Collections;
using UnityEngine;
using UnityEngine.VR;

public class VrManager : MonoBehaviour {

	
	void Awake(){
		StartCoroutine(SwitchToVR());

	}
	
	IEnumerator SwitchToVR(){
		VRSettings.LoadDeviceByName("cardboard");

		// Wait one frame!
		yield return null;

		Debug.Log("here vr");
		// Now it's ok to enable VR mode.
		VRSettings.enabled = true;
	}
	
}
