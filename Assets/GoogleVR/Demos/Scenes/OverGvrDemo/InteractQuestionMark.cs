﻿using System.Collections;
using System.Collections.Generic;
using SocketIO;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class InteractQuestionMark : MonoBehaviour{


	private MeshRenderer myRenderer;
	private SocketManager sockMng;

	private GameObject myCanvasChild;

	private Text textComp;


	private RotateQuestionMark rot;

	private Quaternion childQuaternion;
	
	// Use this for initialization
	void Start (){



		myRenderer = GetComponent<MeshRenderer>();
		rot = GetComponent<RotateQuestionMark>();
		
		sockMng = GameObject.FindGameObjectWithTag("SocketManager").GetComponent<SocketManager>();
		myCanvasChild = transform.GetChild(0).gameObject;
		myCanvasChild.SetActive(false);
		childQuaternion = myCanvasChild.transform.rotation;
		
		
		textComp = myCanvasChild.transform.GetChild(0).gameObject.GetComponent<Text>();


		rot.enabled = false;
		

	}

	public float timeBeforeQuestionAgain =10.0f;
	private float timer = 0.0f;
	
	// Update is called once per frame
	void Update (){
		timer += Time.deltaTime;
	}


	public void OnRecieveCallback(string reply){
		rot.enabled = false;
		StartCoroutine(controlText(reply));

			
			
	
	}
	IEnumerator controlText(string reply){
		
		myCanvasChild.SetActive(true);
		myRenderer.enabled = false;

		textComp.text = reply;
		var fadeTimer = 0.0f;
		
		while (fadeTimer < timeBeforeQuestionAgain){

			
			
			Color c = textComp.color;
			c.a = (timeBeforeQuestionAgain-fadeTimer)/timeBeforeQuestionAgain;
			textComp.color = c;
			
			fadeTimer += Time.deltaTime;
			yield return null;
		}
		myCanvasChild.SetActive(false);
		myRenderer.enabled = true;
		
	}
	
	
	




	private bool firstTime = true;
	public void MouseHitMe(){

		if (sockMng != null  && (firstTime || timer > timeBeforeQuestionAgain) ){
			timer = 0;
			firstTime = false;
			Debug.Log("pinged server");
			sockMng.pingServer();

			rot.enabled = true;


		}
		
	}

	private void LateUpdate(){
		myCanvasChild.transform.rotation = childQuaternion;
		
	}
}
