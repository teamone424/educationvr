﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateQuestionMark : MonoBehaviour{


	private Transform parentTrans;
	public float speed = 1.0f;
	// Use this for initialization
	void Awake (){
		parentTrans = transform.parent.transform;
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		transform.RotateAround(transform.position,parentTrans.up,Time.deltaTime*speed);
		
	}
}
