﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputIPandPort : MonoBehaviour {
	private void Awake(){
		DontDestroyOnLoad(transform.gameObject);
	}
	
	

	// Use this for initialization
	void Start () {
		
		
		Debug.Log(Screen.height+" "+Screen.width);
		
		
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	private bool enabledGui = true;
	public string textFieldString = "127.0.0.1:4567";

	void OnGUI() {

		if (enabledGui){
			Debug.Log("value in text string is " + textFieldString);

			GUI.Label(new Rect(Screen.width / 2.0f - 50, Screen.height / 2.0f - 25, 140, 50), "Enter Details ");
			
			
			
			
			

			textFieldString = GUI.TextField(new Rect(Screen.width / 2.0f - 50, Screen.height / 2.0f + 25, 100, 50),
				textFieldString);

			if (GUI.Button(new Rect(Screen.width / 2.0f - 50, Screen.height / 2.0f + 75, 100, 50), "Enter")){
				enabledGui = false;
				SceneManager.LoadScene("GVRDemo");
			}
		}

	}
}
