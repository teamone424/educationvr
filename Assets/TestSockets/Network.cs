﻿using System;
using SocketIO;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;


[RequireComponent(typeof(SocketIOComponent))]
public class Network : MonoBehaviour{

	private Transform t;
	private SocketIOComponent socket;
	// Use this for initialization
	void Start (){
		t = GetComponent<Transform>();
		socket = GetComponent<SocketIOComponent>();
		Assert.IsTrue(socket!=null);
		socket.On("open",OnConnected);
//		socket.On("spawn",OnSpawn);
//		socket.On("id",OnID);
	}

	void OnID(SocketIOEvent e){
		
		Debug.Log("my id is "+e.data["id"].ToString());
		Debug.Log("my number is "+float.Parse(e.data["num"].ToString()).ToString());
		
		
	}

	
	void OnConnected(SocketIOEvent e){
		
		Debug.Log("connected to server");
//		socket.Emit("move");
//		Debug.Log(" \"Hello World\" ");
		
		
	}

	void OnSpawn(SocketIOEvent e){
//		Debug.Log("spawn event recieved");
		

	}

	string Vector3ToString(Vector3 v){
		return string.Format(" {{  \"x\": \"{0}\" , \"y\": \"{1}\" , \"z\": \"{2}\"  }} ", v.x, v.y, v.z);
		
	}
	// Update is called once per frame
	void Update (){

		t.position+=Vector3.left*Time.deltaTime;
		
		
//		socket.Emit("move",new JSONObject(Vector3ToString(t.position)));
		



	}
}
